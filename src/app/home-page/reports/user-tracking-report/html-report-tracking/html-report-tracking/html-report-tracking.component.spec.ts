import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlReportTrackingComponent } from './html-report-tracking.component';

describe('HtmlReportTrackingComponent', () => {
  let component: HtmlReportTrackingComponent;
  let fixture: ComponentFixture<HtmlReportTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlReportTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlReportTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
