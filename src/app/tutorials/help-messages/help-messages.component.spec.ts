import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpMessagesComponent } from './help-messages.component';

describe('HelpMessagesComponent', () => {
  let component: HelpMessagesComponent;
  let fixture: ComponentFixture<HelpMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
