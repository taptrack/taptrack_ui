import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoInviteUserComponent } from './info-invite-user.component';

describe('InfoInviteUserComponent', () => {
  let component: InfoInviteUserComponent;
  let fixture: ComponentFixture<InfoInviteUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoInviteUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoInviteUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
