import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandBarcodesComponent } from './command-barcodes.component';

describe('CommandBarcodesComponent', () => {
  let component: CommandBarcodesComponent;
  let fixture: ComponentFixture<CommandBarcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandBarcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandBarcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
