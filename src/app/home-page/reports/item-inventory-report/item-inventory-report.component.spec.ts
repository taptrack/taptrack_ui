import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInventoryReportComponent } from './item-inventory-report.component';

describe('ItemInventoryReportComponent', () => {
  let component: ItemInventoryReportComponent;
  let fixture: ComponentFixture<ItemInventoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInventoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInventoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
