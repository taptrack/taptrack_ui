import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../_services/profile.service';
import { Title } from '../../../node_modules/@angular/platform-browser';
import { LocalStorageService } from '../../../node_modules/angular-web-storage';
import { Router } from '../../../node_modules/@angular/router';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { LoginService } from '../../app/_services/login.services';
@Component({
  // selector: 'app-addprofile-url',
  templateUrl: './addprofile-url.component.html',
  styleUrls: ['./addprofile-url.component.css'],
  providers: [ProfileService,LoginService]
})
export class AddprofileUrlComponent implements OnInit {
  title = 'Taptrack User Registration'
  userData: any = {};
  companyData: any = {};
  getUserData: any = [];
  locationFlagshw: any = false;
  type : any = {};
  companyName : any = '';
  getStateData : any = [];
  profileData : any = [];
  successfulSave : boolean = false;
  successfulSave1 : boolean = true;
  usernameshw:any = false;
  locationshw:any = false;
  passwordshw:any = false;
  confirmPasswordshw :any = false;
  userFirstNameshw: any = false;
  userLastNameshw:any = false;
  UserStreet1shw:any = false;
  userCityshw:any = false;
  userZipshw:any = false;
  userMobileshw:any =false;
  emailshw :any = false;
  stateIdshw :any = false;
  mailWrng:any = false;
  userTypeIdshw :any = false;
  companyId: any;
  constructor(private PS: ProfileService,private titleService:Title,
     private localStorage: LocalStorageService, private router: Router, private route: ActivatedRoute,  private LS: LoginService) {

    // this.userData = this.localStorage.get("loginInfo");
    // if (this.userData) {
    //   this.userData = JSON.parse(this.userData);
    //   this.companyData = this.userData['userDetails']['companyIdRelations'];
    //   this.companyName = this.companyData.companyName;
    //   this.type['companyName'] = this.companyName;
    //   this.type['uniqueId'] = this.companyData.uniqueId;
    // }
    //  else {
    //   this.router.navigate(['/login']);
    // }
    this.route.queryParams.subscribe(params => {
      this.LS.getCompanyDetails({'uniqueId' :params.companyUniqueId}).subscribe(data=> {
        this.type.companyName = data['companyName']
        this.companyId = data['companyId'];
      })
      
  })
  }
  emailvalidation = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.getUserypes();
    this.getStates();
  }
  getUserypes() {
    this.PS.getUserType().subscribe(getUserData => {
      this.getUserData = getUserData;
    });
  }
  passwrdmatch:any =false;
  getStates() {
    this.PS.getState().subscribe(getStateData => {
      this.getStateData = getStateData;

    });
  }
  cancelProfile() {
    // this.router.navigate(['/home']);
  }
  errorFormFieldsArr = [];
  userProfileFormFields = [
    "username", "locationFlag", "password", "confirmPassword", "userFirstName", "userLastName",
    "UserStreet1", "userCity", "userZip", "userMobile", "email", "stateId", "userTypeId"
  ]
  createProfile() {    
    this.errorFormFieldsArr = [];
    this.userProfileFormFields.forEach((item)=>{
      if (item == 'locationFlag') {
        if (this.type['locationFlag'] == undefined || this.type['locationFlag'] == '' || this.type['locationFlag'] == '') {
          this.locationFlagshw = false;
        }
        if (this.type['locationFlag']) {
          if (this.type['location']) {
            this.locationFlagshw = false;
          } else {
            this.errorFormFieldsArr.push(item);
            this.locationFlagshw = true;
          }
    
        }
      } else {
      if(this.type[item] == null || this.type[item] == ''){
        this.errorFormFieldsArr.push(item);
        this[item + "shw"] = true;
      }else{
        this[item + "shw"] = false;
      }
    }
    });
    // if(this.type.password != this.type.confirmPassword){
    //   this.passwrdmatch = true;
    //   return;
      
    // }else{
    //   this.passwrdmatch = false;
    // }
    if(this.type.password != this.type.confirmPassword){
      this.passwrdmatch = true;
      return;
      
    }else{
      this.passwrdmatch = false;
    }
    if(!this.emailvalidation.test(this.type.email)){
           this.mailWrng = true;
      return;
    }else{
      this.mailWrng = false;
    }
    if(this.errorFormFieldsArr.length > 0){
      return;
    };
    let profileInput = {};
    profileInput = this.type;
    profileInput['companyId'] = this.companyId;
    profileInput['toolTipFlag'] = 0;
    profileInput['userApproved'] = 0;
    profileInput['userMI'] = '';
    profileInput['userPrefix'] = '';
    profileInput['userStatus'] = 0;
    // profileInput['userStatus'] = 1;
    profileInput['locationFlag'] = (this.type.locationFlag) ? 1 : 0;
    let idd = this.type['userTypeId']
    let usrtype = {};
    usrtype =  this.getUserData.find(getID);
    profileInput['userType'] =usrtype['userType'];
    function getID(obj) {
      if (obj.userTypeId ==idd) {
        return obj.userType
      }
      
    }
    if (profileInput['password'] == profileInput['confirmPassword']) {
      this.PS.createProfile(profileInput).subscribe(profileData => {
        this.profileData = profileData;
        this.type = {};
        this.successfulSave = true;
        this.successfulSave1 = false;
      },
      error =>{
      }
      
    );
    }
  }
  isNumberKey(evt) {

    if (isNaN(evt)) {

      this.type.userZip = ''

    }

  }
  isDisabled = true;
  triggerSomeEvent() {
      this.isDisabled = !this.isDisabled;
      return;
  }
}
