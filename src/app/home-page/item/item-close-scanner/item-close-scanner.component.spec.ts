import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCloseScannerComponent } from './item-close-scanner.component';

describe('ItemCloseScannerComponent', () => {
  let component: ItemCloseScannerComponent;
  let fixture: ComponentFixture<ItemCloseScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCloseScannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCloseScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
