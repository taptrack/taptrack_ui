import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeItemStatusComponent } from './change-item-status.component';

describe('ChangeItemStatusComponent', () => {
  let component: ChangeItemStatusComponent;
  let fixture: ComponentFixture<ChangeItemStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeItemStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeItemStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
