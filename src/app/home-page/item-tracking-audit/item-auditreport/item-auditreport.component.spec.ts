import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAuditreportComponent } from './item-auditreport.component';

describe('ItemAuditreportComponent', () => {
  let component: ItemAuditreportComponent;
  let fixture: ComponentFixture<ItemAuditreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemAuditreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemAuditreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
