import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { Title } from '../../../node_modules/@angular/platform-browser';

@Component({
  selector: 'app-barcode-management',
  templateUrl: './barcode-management.component.html',
  styleUrls: ['./barcode-management.component.css']
})
export class BarcodeManagementComponent implements OnInit {
  title = 'Barcode Management'
  uniqueId : any = '';
  constructor(private route: ActivatedRoute, private router: Router, private titleService: Title) { }
  companyId: any;
   /**ngOnInit method
   * @constructor
   * the method to call on component loading
   */
  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.route.queryParams.subscribe(params => {
      this.companyId = params.companyId;
  })
  }
  /** location method to navigate to Location Barcodes page on clicking of Location Barcodes */
  loaction(){
    this.router.navigate(['/locationBarcodes'], { queryParams: { companyId: this.companyId } });
  }
  /** user method to navigate to User Barcodes page on clicking of User Barcodes */
  user(){
    this.router.navigate(['/userBarcodes'], { queryParams: { uniqueId: this.companyId } });
  }

  add(){
    this.router.navigate(['/addProfile'], { queryParams: { companyId: this.companyId } });
  }
  /** user method to navigate toTap Track Easy Scanning page on clicking of Tap Track Easy Scanning */
  easyScanning(){
    this.router.navigate(['/tapTrackEasyScanning'], { queryParams: { companyId: this.companyId } });
  }
  /** user method to navigate toTap Track Item Inventory Report page on clicking of Tap Track Item Inventory Report */
  inventory(){
    this.router.navigate(['/itemInventor'], { queryParams: { companyId: this.companyId } });
  }
}
