import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddprofileUrlComponent } from './addprofile-url.component';

describe('AddprofileUrlComponent', () => {
  let component: AddprofileUrlComponent;
  let fixture: ComponentFixture<AddprofileUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddprofileUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddprofileUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
