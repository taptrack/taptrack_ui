import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpAddItemTypeComponent } from './help-add-item-type.component';

describe('HelpAddItemTypeComponent', () => {
  let component: HelpAddItemTypeComponent;
  let fixture: ComponentFixture<HelpAddItemTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpAddItemTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpAddItemTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
