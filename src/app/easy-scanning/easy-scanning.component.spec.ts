import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyScanningComponent } from './easy-scanning.component';

describe('EasyScanningComponent', () => {
  let component: EasyScanningComponent;
  let fixture: ComponentFixture<EasyScanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EasyScanningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyScanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
