import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintUniqiebarcodesComponent } from './print-uniqiebarcodes.component';

describe('PrintUniqiebarcodesComponent', () => {
  let component: PrintUniqiebarcodesComponent;
  let fixture: ComponentFixture<PrintUniqiebarcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintUniqiebarcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintUniqiebarcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
