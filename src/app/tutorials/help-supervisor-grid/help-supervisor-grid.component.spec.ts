import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpSupervisorGridComponent } from './help-supervisor-grid.component';

describe('HelpSupervisorGridComponent', () => {
  let component: HelpSupervisorGridComponent;
  let fixture: ComponentFixture<HelpSupervisorGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpSupervisorGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpSupervisorGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
