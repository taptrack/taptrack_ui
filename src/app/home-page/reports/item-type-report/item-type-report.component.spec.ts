import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemTypeReportComponent } from './item-type-report.component';

describe('ItemTypeReportComponent', () => {
  let component: ItemTypeReportComponent;
  let fixture: ComponentFixture<ItemTypeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemTypeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemTypeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
