import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorsGridComponent } from './supervisors-grid.component';

describe('SupervisorsGridComponent', () => {
  let component: SupervisorsGridComponent;
  let fixture: ComponentFixture<SupervisorsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
