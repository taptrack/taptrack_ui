import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlReportSentbyComponent } from './html-report-sentby.component';

describe('HtmlReportSentbyComponent', () => {
  let component: HtmlReportSentbyComponent;
  let fixture: ComponentFixture<HtmlReportSentbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlReportSentbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlReportSentbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
