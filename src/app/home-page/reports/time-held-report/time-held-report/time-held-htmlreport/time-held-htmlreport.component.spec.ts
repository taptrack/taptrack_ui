import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeHeldHtmlreportComponent } from './time-held-htmlreport.component';

describe('TimeHeldHtmlreportComponent', () => {
  let component: TimeHeldHtmlreportComponent;
  let fixture: ComponentFixture<TimeHeldHtmlreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeHeldHtmlreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeHeldHtmlreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
