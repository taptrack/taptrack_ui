import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindersProcessedReportComponent } from './binders-processed-report.component';

describe('BindersProcessedReportComponent', () => {
  let component: BindersProcessedReportComponent;
  let fixture: ComponentFixture<BindersProcessedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindersProcessedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindersProcessedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
