import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpPendingUsersComponent } from './help-pending-users.component';

describe('HelpPendingUsersComponent', () => {
  let component: HelpPendingUsersComponent;
  let fixture: ComponentFixture<HelpPendingUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpPendingUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpPendingUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
