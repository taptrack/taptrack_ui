import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlmailreportComponent } from '../htmlmailreport/htmlmailreport.component';

describe('HtmlmailreportComponent', () => {
  let component: HtmlmailreportComponent;
  let fixture: ComponentFixture<HtmlmailreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlmailreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlmailreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
