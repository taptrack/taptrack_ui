import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlReportInventoryComponent } from './html-report-inventory.component';

describe('HtmlReportInventoryComponent', () => {
  let component: HtmlReportInventoryComponent;
  let fixture: ComponentFixture<HtmlReportInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlReportInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlReportInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
