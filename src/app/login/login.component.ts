import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../app/_services/login.services';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'angular-web-storage';
import * as _ from 'lodash';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  title ='Tap Track :: Login';
  usernameshw:any =false;
  passwordshw :any = false;
  wrngdetails :any = false;
  loginForm : any = {};
  allCompanynames: any = [];
  allCompanynames1: any = [];
  forgotShow :any =false;
  loginResponse: any = {};
  KEY = 'loginInfo';
  errorFormFieldsArr = [];
  userProfileFormFields = [
    "username", "password"
  ]

  /** login method : 
   * @param {String} username - username of the user
   * @param {String} password - password
   * @param {Number} companyId - companyId
   */
  login(x) {
    this.usernameshw = false;
    this.passwordshw = false;
    this.wrngdetails = false;
    this.errorFormFieldsArr = [];
    this.userProfileFormFields.forEach((item)=>{
      if(this.loginForm[item] == null || this.loginForm[item] == ''){
        this.errorFormFieldsArr.push(item);
        this[item + "shw"] = true;
      }else{
        this[item + "shw"] = false;
      }
    });
    if(this.errorFormFieldsArr.length > 0){
      return;
    };


    // if(this.loginForm.username == null || this.loginForm.username == '' && this.loginForm.password == null || this.loginForm.password == ''){
    //   this.usernameshow = true;
    //   this.passwordshow = true;
    //   this.wrngdetails = false;
    //   return
    //     }else if(this.loginForm.username == null || this.loginForm.username == ''){
    //       this.usernameshow = true;
    //       this.passwordshow = false;
    //       this.wrngdetails = false;
    //     }else if(this.loginForm.password == null || this.loginForm.password == ''){
    //       this.usernameshow = false;
    //       this.passwordshow = true;
    //       this.wrngdetails = false;
    //     }
    // else {
    //   this.usernameshow = false;
    //   this.passwordshow = false;

    /** calling login api 
     *@param {Object} this.loginForm is an input
     * loginData  is a output from an login api
     * @param {Object} this.loginResponse - this object will hold the response.
    */
    this.LS.login(this.loginForm).subscribe(loginData => {
      this.loginResponse = loginData;
      /** if login response exist it will allow you to enter into the application otherwise if it throw an error and it will be in login page only. */
      if (this.loginResponse) {
        // this.local.clear();
        this.local.set(this.KEY, JSON.stringify(this.loginResponse), 30, 'm');
        this.router.navigate(['/home'])
      } else {
        this.router.navigate(['/login'])
      }


    },
    /** throws an error if login credentials does not match */
    error => {
      if (error.error) {
        this.wrngdetails = true;
        this.usernameshw = false;
        this.passwordshw = false;
      }
      else if (error.error.error.statusCode == "401") {
        this.wrngdetails = true;
        this.usernameshw = false;
        this.passwordshw = false;
        return;
      }else{
        this.wrngdetails = false;
      }
    }
  );
  // }
  }
  constructor( private LS: LoginService,private titleService: Title, private router: Router,public local: LocalStorageService,public session: SessionStorageService){

  }
  /**ngOnInit method
   * @constructor
   * the method to call on component loading
   */
  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.allCompanies();

  }
  companayvalue:any;
  selectcompany(event){
    this.companayvalue =event.target.value;
  }
  forgot(){
    this.forgotShow = true;
  }
  companyId:any;
  /** allCompanies method:
   * @constructor
   * the method to fetch company information and also it will filter Norcross company not to include in the response.
   */
  allCompanies() {
    var self =this
    self.companyId = self.companayvalue;
    /** getCompanies api calling */
    self.LS.getCompanies().subscribe(
        data => {
        self.loginForm.companyId = data[1].companyId
        self.allCompanynames1 = data;
        _.each(self.allCompanynames1, function (datas) {
          // console.log(datas)
          if (datas.companyId != 72) { /** Dont show 'Norcross' on UI - hence filtering**/
            self.allCompanynames.push(datas)
          } else {
            // console.log(data)
          }
        })
      },
        error => {

      }
    )

  }
/** comapnyRout method:
 * on clicking on company it will move to barcode management.
 */
  comapnyRout(id){
    this.router.navigate(['/barCodeManagement'], { queryParams: { companyId: id } });
  }

  // comapnyRout(id){
  //   this.router.navigate(['/barCodeManagement/'+id]);
  // }
}
