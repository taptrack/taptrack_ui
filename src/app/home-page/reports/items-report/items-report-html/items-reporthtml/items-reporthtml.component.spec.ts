import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsReporthtmlComponent } from './items-reporthtml.component';

describe('ItemsReporthtmlComponent', () => {
  let component: ItemsReporthtmlComponent;
  let fixture: ComponentFixture<ItemsReporthtmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsReporthtmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsReporthtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
