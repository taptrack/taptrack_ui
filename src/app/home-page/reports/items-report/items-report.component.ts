import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ItemService } from "../../../_services/item.service";
import { UserService } from "../../../_services/user.service";
import * as _ from 'lodash';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable';
import { ActivatedRoute } from '@angular/router';

import {
  LocalStorageService,
  SessionStorageService,
  LocalStorage,
  SessionStorage
} from "angular-web-storage";
import { Router } from "@angular/router";
import { error } from '@angular/compiler/src/util';
@Component({
  selector: 'app-items-report',
  templateUrl: './items-report.component.html',
  styleUrls: ['./items-report.component.css'],
  providers: [ItemService, UserService]

})
export class ItemsReportComponent implements OnInit {
  title = 'TapTrack Items Report';
  userData: any = []
  companyData: any = []
  itemList: any = [];
  reqErr: any = false;
  ItemNumber: any
  itemId: any
  data: any;
  Valid: any;
  error2: any = false;
  error: any = false
  constructor(private titleService: Title, private IS: ItemService, private route: ActivatedRoute,
    private US: UserService,
    private localStorage: LocalStorageService,
    private router: Router, ) {
    this.userData = this.localStorage.get("loginInfo");
    if (this.userData) {
      this.userData = JSON.parse(this.userData);
      this.companyData = this.userData["userDetails"]["companyIdRelations"];
    } else {
    }
  }

  ngOnInit() {

    this.titleService.setTitle(this.title);
    let input = {};
    input = {
      companyId: this.companyData.companyId
    };
    this.IS.getItemType({ companyId: this.companyData.companyId }).subscribe(itemList => {
      this.itemList = itemList;
    });
  }
  generatehtml(ItemNumber, itemId) {
    if ((ItemNumber && itemId) && ItemNumber != undefined && itemId != undefined) {
      let input = {
        companyId: this.companyData.companyId,
        itemUniqueId: ItemNumber,
        itemTypeId: itemId
      }
      this.US.validate(input).subscribe(count => {
        this.Valid = count
        if (this.Valid > 0) {
          this.error2 = false
          this.error = false

          var query = {
            companyId: this.companyData.companyId,
            itemUniqueId: ItemNumber,
            itemTypeId: itemId
          }
          window.open(`#/hrmlReportitemsreport?query=${JSON.stringify(query)}`, 'name', 'width=800,height=600,scrollbars=1');
        } else {
          this.error2 = true
          this.error = false
          return
        }
      })
    }
    else if (ItemNumber) {

      this.US.findexistance({ itemUniqueId: ItemNumber, companyId: this.companyData.companyId }).subscribe(usrData => {
        this.data = usrData
        if (this.data > 0) {
          this.error = false

          var query = {
            companyId: this.companyData.companyId,
            itemUniqueId: ItemNumber,
            itemTypeId: itemId
          }
          window.open(`#/hrmlReportitemsreport?query=${JSON.stringify(query)}`, 'name', 'width=800,height=600,scrollbars=1');
        } else {
          this.error = true
          this.error2 = false

        }
      })
    } else if (itemId) {
      var query = {
        companyId: this.companyData.companyId,
        itemTypeId: itemId
      }
      window.open(`#/hrmlReportitemsreport?query=${JSON.stringify(query)}`, 'name', 'width=800,height=600,scrollbars=1');
    } else {
      alert('Please Select Fields')
    }

  }
  clearfields() {
    this.ItemNumber = ''
    this.itemId = ''
    this.error = false
    this.error2 = false
    this.reqErr = false
  }

  itmList: any = [];
  getPdf() {
    let itemInput = {};
    let input = {}
    if (this.ItemNumber && (this.itemId == undefined || this.itemId == '')) {
      this.US.findexistance({ itemUniqueId: this.ItemNumber, companyId: this.companyData.companyId }).subscribe(usrData => {
        this.data = usrData
        if (this.data > 0) {
          this.error2 = false
          input = {
            companyId: this.companyData.companyId,
            itemUniqueId: this.ItemNumber,
            itemTypeId: this.itemId
          }
          this.getResults(input);
        } else {
          this.error = true
          this.error2 = false

        }
      })
    } else
      if ((this.ItemNumber && this.itemId) && this.ItemNumber != undefined && this.itemId != undefined) {

        input = {
          companyId: this.companyData.companyId,
          itemUniqueId: this.ItemNumber,
          itemTypeId: this.itemId
        }
        this.US.validate(input).subscribe(count => {
          this.Valid = count
          if (this.Valid > 0) {
            this.error2 = false;
            this.getResults(input);
          } else {
            this.error = false
            this.error2 = true
          }

        });
      } else if (this.ItemNumber || this.itemId) {
        console.log(this.itemId)
        console.log(this.ItemNumber)
        if (this.ItemNumber) {
          input = {
            companyId: this.companyData.companyId,
            itemUniqueId: this.ItemNumber,
          }
          this.getResults(input);

        } else if (this.itemId) {
          input = {
            companyId: this.companyData.companyId,
            itemTypeId: this.itemId
          }
          this.getResults(input);
        }

      } else {
        this.error = false;
        this.reqErr = true;
        this.error2 = false;
      }


  }

  itemReport: any = [];
  getResults(input) {
    this.US.getitemsreportsarray(input).subscribe(usrData => {
      var daysHeldArr = [];
      this.itemReport = []
      if (usrData != null && usrData != undefined) {
        this.error = false;
        this.error2 = false;
        this.itmList = usrData[0];

        for (var i = 0; i < this.itmList.itemArrList.length; i++) {
          let objj = {};
          objj = this.itmList.itemArrList[i];
          let date = new Date(this.itmList.itemArrList[i].dateCreated);
          let firtsname = this.itmList.itemArrList[i].itemHolderDetails ? this.itmList.itemArrList[i].itemHolderDetails.userFirstName : ''
          let lastname = this.itmList.itemArrList[i].itemHolderDetails ? this.itmList.itemArrList[i].itemHolderDetails.userLastName : ''
          let users =  this.itmList.itemArrList[i].itemHolderDetails ? this.itmList.itemArrList[i].itemHolderDetails.username : ''
          let month = (date.getMonth() + 1);
          let datee = date.getDate();
          objj['holderName'] = firtsname + ' ' + lastname + '(' + users + ')'
          objj['dateCreated'] = date.getFullYear() + '-' + ((month.toString().length == 1) ? ('0' + month) : month) + '-' + ((datee.toString().length == 1) ? ('0' +datee) : datee) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds();
          this.itemReport.push(objj);

        }
        const doc = new jsPDF();
        // doc.autoTable({ html: '#my-table' });
        doc.setFontSize(10);
        // doc.setFontStyle('arial');
        // doc.text(11, 12, "Items Report");
        // doc.text(17, 18, "Items Count =       " + this.itmList.length);
        doc.text(20, 35, "Item Type :       " + this.itmList.itemArrList[0].itemType);
        doc.text(140, 35, "Item Total   :    " + this.itmList.noOfItems);
        doc.autoTable({
          styles: { width: 60, border: 2, halign: 'center' },
          columnStyles: { text: { cellWidth: 'auto', border: 2 } }, // European countries centered
          body: this.itemReport,
          columns: [{ header: 'Item Number', dataKey: 'itemNumber' }, { header: 'Date Created', dataKey: 'dateCreated' }, { header: 'Tracking Item', dataKey: 'trackingItem' }, { header: 'Item Holder', dataKey: 'holderName' }, { header: 'Days In Tracking', dataKey: 'daysinTracking' }],
          margin: { top: 40, bottom: 20 },
          tableLineWidth: 0.5,
          theme: "grid",
          headStyles: {
            fillColor: [255, 255, 255],
            fontSize: 10,
            textColor: [25, 25, 25],
            theme: "plane",
            marginBottom: 20,
            lineWidth: 0.5,
            marginTop: 20,
            border: 4,
            // rowHeight: 6,
            lineColor: 200
          },
          beforePageContent: function (data) {
            doc.setFontSize(20), doc.setFontStyle("bold"), doc.text("Items Report", 75, 20);
          }
        });
        doc.save('ItemsReport.pdf');
      } else {
        this.error = true;
        this.error2 = false;
      }
    });
  }
}
