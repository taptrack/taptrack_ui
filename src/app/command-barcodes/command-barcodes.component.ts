import { Component, OnInit } from '@angular/core';
import { UserLocationService } from '../../app/_services/user.location.service';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Title } from '../../../node_modules/@angular/platform-browser';
@Component({
  selector: 'app-command-barcodes',
  templateUrl: './command-barcodes.component.html',
  styleUrls: ['./command-barcodes.component.css'],
  providers: [UserLocationService]
})
export class CommandBarcodesComponent implements OnInit {
  commandbar: any;
  title = 'Print All Barcodes';

  constructor(private titleService: Title, private BS: UserLocationService, private route: ActivatedRoute) { }
  /**ngOnInit method
      * @constructor
      * the method to call on component loading
      */
  ngOnInit() {
    this.titleService.setTitle(this.title);
    // this.getcommandBarCode();

  }
  // getcommandBarCode() {    
  //   this.BS.getcommandBarCodes().subscribe(data => {        
  //       this.commandbar = data;
  //   });
  // }


}
