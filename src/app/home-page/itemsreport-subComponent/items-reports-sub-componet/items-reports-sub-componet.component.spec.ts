import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsReportsSubComponetComponent } from './items-reports-sub-componet.component';

describe('ItemsReportsSubComponetComponent', () => {
  let component: ItemsReportsSubComponetComponent;
  let fixture: ComponentFixture<ItemsReportsSubComponetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsReportsSubComponetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsReportsSubComponetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
