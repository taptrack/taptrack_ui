import { Component, OnInit } from '@angular/core';
import { supervisorService } from '../../_services/supervisor.service';
import { LocalStorageService } from '../../../../node_modules/angular-web-storage';
import { ItemService } from '../../_services/item.service';
import { Title } from '../../../../node_modules/@angular/platform-browser';

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.component.html',
  styleUrls: ['./home-main.component.css'],
  providers: [ItemService]
})
export class HomeMainComponent implements OnInit {
  title ='TapTrack Home'
  constructor(private ss : supervisorService, public local: LocalStorageService, private IS: ItemService,private titleService: Title) { }
  loginForm:any ={};
  messageData:any ;
  userData1 :any;
  companyId:any;
  /**ngOnInit method
      * @constructor
      * the method to call on component loading
      */
  ngOnInit() {
    this.titleService.setTitle(this.title);

    this.userData1 = this.local.get("loginInfo");
    if (this.userData1) {      
      this.userData1 = JSON.parse(this.userData1);
       this.companyId = this.userData1.userDetails.companyId;
    }
    this.messages();
    this.countofAllItems();
  }
  showmessageTable :any = false;
  /** messages Method :
   * @constructor
   * @param {Object} input - input for getCompanyMessages
   * @param {Array} messageData - output for getCompanyMessages
   * get messages based on companyId
   */
  messages() {
    let input = {};
    input = {
      'companyId':this.companyId,
      "status": 1
    }
    /** getCompanyMessages api calling */
    this.ss.getMessages(input).subscribe((userData :any) => {
      if (userData != null) {       
        this.messageData = userData;
        if(this.messageData.length>0){
          this.showmessageTable = true;
        }else{
          this.messageData = false;
        }
      }
    });
    
  }
  count:any;
  /** countofAllItems Method : 
   * @constructor
   * @param {Object} input - input for getItemsCount service
   * It will return the list of items( casebinder )
   */
  countofAllItems() {
    let input = {};
    input = {
      'companyId':this.companyId
    }
    /** getItemsCount api calling */
    this.IS.countofItems(input).subscribe((userData :any) => {
      if (userData != null) {       
        this.count = userData;
      }
    });
  }
}
