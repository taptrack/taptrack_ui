import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditItemListtagComponent } from './edit-item-listtag.component';

describe('EditItemListtagComponent', () => {
  let component: EditItemListtagComponent;
  let fixture: ComponentFixture<EditItemListtagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditItemListtagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditItemListtagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
