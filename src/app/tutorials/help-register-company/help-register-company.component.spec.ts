import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpRegisterCompanyComponent } from './help-register-company.component';

describe('HelpRegisterCompanyComponent', () => {
  let component: HelpRegisterCompanyComponent;
  let fixture: ComponentFixture<HelpRegisterCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpRegisterCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpRegisterCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
