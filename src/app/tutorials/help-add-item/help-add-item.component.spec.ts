import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpAddItemComponent } from './help-add-item.component';

describe('HelpAddItemComponent', () => {
  let component: HelpAddItemComponent;
  let fixture: ComponentFixture<HelpAddItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpAddItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
