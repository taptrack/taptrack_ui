export class AppURL {
      // *** In the master branch do not change or check in this file. This file should be checked out AS-IS and modified
      // *** in respective locals
       public static serverUrl = 'https://taptrac.com/api/'
       public static uiUrl = "https://taptrac.com/"
}
