import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReporthtmlComponent } from './item-reporthtml.component';

describe('ItemReporthtmlComponent', () => {
  let component: ItemReporthtmlComponent;
  let fixture: ComponentFixture<ItemReporthtmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemReporthtmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReporthtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
