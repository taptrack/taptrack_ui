import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInventorComponent } from './item-inventor.component';

describe('ItemInventorComponent', () => {
  let component: ItemInventorComponent;
  let fixture: ComponentFixture<ItemInventorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInventorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInventorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
