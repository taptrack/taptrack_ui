import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCloseComponent } from './item-close.component';

describe('ItemCloseComponent', () => {
  let component: ItemCloseComponent;
  let fixture: ComponentFixture<ItemCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
