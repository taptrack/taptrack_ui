import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeHeldReportComponent } from './time-held-report.component';

describe('TimeHeldReportComponent', () => {
  let component: TimeHeldReportComponent;
  let fixture: ComponentFixture<TimeHeldReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeHeldReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeHeldReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
