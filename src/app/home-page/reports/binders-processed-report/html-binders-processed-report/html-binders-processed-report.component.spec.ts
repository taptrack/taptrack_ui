import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlBindersProcessedReportComponent } from './html-binders-processed-report.component';

describe('HtmlBindersProcessedReportComponent', () => {
  let component: HtmlBindersProcessedReportComponent;
  let fixture: ComponentFixture<HtmlBindersProcessedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlBindersProcessedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlBindersProcessedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
