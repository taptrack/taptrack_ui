import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpChangeItemStatusComponent } from './help-change-item-status.component';

describe('HelpChangeItemStatusComponent', () => {
  let component: HelpChangeItemStatusComponent;
  let fixture: ComponentFixture<HelpChangeItemStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpChangeItemStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpChangeItemStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
