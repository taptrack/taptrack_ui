import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationBarcodesComponent } from './location-barcodes.component';

describe('LocationBarcodesComponent', () => {
  let component: LocationBarcodesComponent;
  let fixture: ComponentFixture<LocationBarcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationBarcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationBarcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
