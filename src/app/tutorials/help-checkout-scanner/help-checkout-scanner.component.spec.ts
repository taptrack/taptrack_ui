import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpCheckoutScannerComponent } from './help-checkout-scanner.component';

describe('HelpCheckoutScannerComponent', () => {
  let component: HelpCheckoutScannerComponent;
  let fixture: ComponentFixture<HelpCheckoutScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpCheckoutScannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpCheckoutScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
