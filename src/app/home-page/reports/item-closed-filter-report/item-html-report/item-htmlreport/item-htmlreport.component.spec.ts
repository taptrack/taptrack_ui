import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemHtmlreportComponent } from './item-htmlreport.component';

describe('ItemHtmlreportComponent', () => {
  let component: ItemHtmlreportComponent;
  let fixture: ComponentFixture<ItemHtmlreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemHtmlreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemHtmlreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
