import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSearchReportComponent } from './item-search-report.component';

describe('ItemSearchReportComponent', () => {
  let component: ItemSearchReportComponent;
  let fixture: ComponentFixture<ItemSearchReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSearchReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSearchReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
