import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlReportMailComponent } from './html-report-mail.component';

describe('HtmlReportMailComponent', () => {
  let component: HtmlReportMailComponent;
  let fixture: ComponentFixture<HtmlReportMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlReportMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlReportMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
