import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCheckoutScannerComponent } from './item-checkout-scanner.component';

describe('ItemCheckoutScannerComponent', () => {
  let component: ItemCheckoutScannerComponent;
  let fixture: ComponentFixture<ItemCheckoutScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCheckoutScannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCheckoutScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
