import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoForgotComponent } from './info-forgot.component';

describe('InfoForgotComponent', () => {
  let component: InfoForgotComponent;
  let fixture: ComponentFixture<InfoForgotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoForgotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoForgotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
