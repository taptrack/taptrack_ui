import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintBarCodesComponent } from './print-bar-codes.component';

describe('PrintBarCodesComponent', () => {
  let component: PrintBarCodesComponent;
  let fixture: ComponentFixture<PrintBarCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintBarCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintBarCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
