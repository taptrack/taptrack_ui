import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemClosedFilterReportComponent } from './item-closed-filter-report.component';

describe('ItemClosedFilterReportComponent', () => {
  let component: ItemClosedFilterReportComponent;
  let fixture: ComponentFixture<ItemClosedFilterReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemClosedFilterReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemClosedFilterReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
