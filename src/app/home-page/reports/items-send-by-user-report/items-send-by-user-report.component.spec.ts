import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsSendByUserReportComponent } from './items-send-by-user-report.component';

describe('ItemsSendByUserReportComponent', () => {
  let component: ItemsSendByUserReportComponent;
  let fixture: ComponentFixture<ItemsSendByUserReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsSendByUserReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsSendByUserReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
