import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBarcodesComponent } from './user-barcodes.component';

describe('UserBarcodesComponent', () => {
  let component: UserBarcodesComponent;
  let fixture: ComponentFixture<UserBarcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBarcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBarcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
