import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
// import { DatePipe } from '@angular/common';
import * as _ from 'lodash';
@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() {

  }

  async generateExcel(response) {

    // const ExcelJS = await import('exceljs');
    // console.log(ExcelJS);
    // const Workbook: any = {};

  // Excel Title, Header, Data
    const title = 'Binders Processed Report';
  const header = ['Item', 'Sender', 'Sender To Date', 'Holder']
  const data1 = [] = response;
 

        const data = [];
        let data2 = [];
        let data3 = [];
          // var self = this
          // _.each(data1, function (obj) {
          //   data2=[];
          //   data2.push(obj.itemUniqueId)
          //   data2.push(obj.senderName)
          //   data2.push(obj.transactionSuccessTime)
          //   data2.push(obj.recieverName)
          //   data.push(data2)
          // })

          for(var i=0;i<data1.length;i++) {
            data2=[];
            let obj  = data1[i]
            if(i==0) {
              data2.push(obj.itemUniqueId)
              data2.push(obj.senderName)
              data2.push(obj.transactionSuccessTime)
              data2.push(obj.recieverName)
              data.push(data2)
            } else if (i>0 && data1[i-1].itemUniqueId==data1[i].itemUniqueId) {
              data2.push(null)
              data2.push(obj.senderName)
              data2.push(obj.transactionSuccessTime)
              data2.push(obj.recieverName)
              data.push(data2)
            } else {
              data2.push(obj.itemUniqueId)
              data2.push(obj.senderName)
              data2.push(obj.transactionSuccessTime)
              data2.push(obj.recieverName)
              data.push(data2)
            }
          }
          
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Binders Processed Report');


// Add Row and formatting
    const titleRow = worksheet.addRow([title]);
    titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, underline: 'double', bold: true };
    // worksheet.addRow('Items Count: ' + 100)
    worksheet.addRow([]);
    // const subTitleRow = worksheet.addRow(['Date : ' + this.datePipe.transform(new Date(), 'medium')]);
    const subTitleRow = worksheet.addRow(['Items Count : ' +100]);


// Add Image
//     const logo = workbook.addImage({
//   base64: logoFile.logoBase64,
//   extension: 'png',
// });

    // worksheet.addImage(logo, 'E1:F3');
    // worksheet.mergeCells('A1:D2');


// Blank Row
    worksheet.addRow([]);

// Add Header Row
    const headerRow = worksheet.addRow(header);

// Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
  cell.fill = {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: 'FFFFFF00' },
    bgColor: { argb: 'FF0000FF' }
  };
  cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
});
// worksheet.addRows(data);


// Add Data and Conditional Formatting
    data.forEach(d => {
  const row = worksheet.addRow(d);
 
  // row.eachCell((cell, number) => {
  //   if(d[0]==null) {
  //     var res = cell['_address'].split("B");
  //     console.log('dddddddddddddddddddddd ', res, number);
  //     if(res[0]=='') {

  //     var aa = 'A'+res[1]
  //     var aaa = 'A'+(res[1]-1)
  //     console.log('-------------------------------',aa, worksheet.getCell(aa).value, worksheet.getCell(aaa).value);
     
  //     // worksheet.mergeCells('A'+res[1], 'A'+res[1])
  //     }
  //   }
  // })
  
  
  // const qty = row.getCell(5);
  let color = 'FF99FF99';
  // if (+qty.value < 500) {
  //   color = 'FF9999';
  // }

  // qty.fill = {
  //   type: 'pattern',
  //   pattern: 'solid',
  //   fgColor: { argb: color }
  // };
}

);
    worksheet.getColumn(1).width = 30;
    worksheet.getColumn(2).width = 30;
    worksheet.getColumn(3).width = 30;
    worksheet.getColumn(4).width = 30;
    worksheet.addRow([]);


// Footer Row
    const footerRow = worksheet.addRow(['This is system generated excel sheet.']);
    footerRow.getCell(1).fill = {
  type: 'pattern',
  pattern: 'solid',
  fgColor: { argb: 'FFCCFFE5' }
};
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

// Merge Cells
    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
    // worksheet.mergeCells('A2984:A2987')
// Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data: any) => {
  const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
  fs.saveAs(blob, 'BindersProcessedReport.xlsx');
});

  }
}
