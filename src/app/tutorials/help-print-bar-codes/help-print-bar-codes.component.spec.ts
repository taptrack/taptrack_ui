import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpPrintBarCodesComponent } from './help-print-bar-codes.component';

describe('HelpPrintBarCodesComponent', () => {
  let component: HelpPrintBarCodesComponent;
  let fixture: ComponentFixture<HelpPrintBarCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpPrintBarCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpPrintBarCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
