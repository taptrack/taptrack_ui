import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPackageItemComponent } from './add-package-item.component';

describe('AddPackageItemComponent', () => {
  let component: AddPackageItemComponent;
  let fixture: ComponentFixture<AddPackageItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPackageItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPackageItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
