import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActivityHtmlreportComponent } from './user-activity-htmlreport.component';

describe('UserActivityHtmlreportComponent', () => {
  let component: UserActivityHtmlreportComponent;
  let fixture: ComponentFixture<UserActivityHtmlreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActivityHtmlreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActivityHtmlreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
