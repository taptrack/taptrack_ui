import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTrackingReportComponent } from './user-tracking-report.component';

describe('UserTrackingReportComponent', () => {
  let component: UserTrackingReportComponent;
  let fixture: ComponentFixture<UserTrackingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTrackingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTrackingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
