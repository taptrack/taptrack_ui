import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../_services/profile.service';
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'angular-web-storage';
import { Router } from '@angular/router';
import { Title } from '../../../../../node_modules/@angular/platform-browser';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
  providers: [ProfileService]
})
export class EditProfileComponent implements OnInit {
  title ='TapTrack Edit Profile';
  userData: any = {};
  companyData: any = {};
  getUserData: any = [];
  type : any = {};
  companyName : any = '';
  getStateData : any = [];
  profileData : any = [];
  userType : any;
  successfulSave : boolean = false;
  successfulSave1 : boolean = true;
  usernameshw:any = false;
  locationshw:any = false;
  passwordshw:any = false;
  confirmPasswordshw :any = false;
  userFirstNameshw: any = false;
  userLastNameshw:any = false;
  UserStreet1shw:any = false;
  userCityshw:any = false;
  userZipshw:any = false;
  userMobileshw:any =false;
  emailshw :any = false;
  locationFlagshw:any =false;
  stateIdshw :any = false;
  userTypeIdshw :any = false;
  mailWrng:any = false;
  passwrdmatch:any =false;
  constructor(private PS: ProfileService,private titleService:Title, private localStorage: LocalStorageService, private router: Router) { 
    this.userData = this.localStorage.get("loginInfo");
    if (this.userData) {
      
      this.userData = JSON.parse(this.userData);
      this.type = this.userData['userDetails'];
      this.companyData = this.userData['userDetails']['companyIdRelations'];
      this.companyName = this.companyData.companyName;
      this.type['companyName'] = this.companyName;
    } else {
      this.router.navigate(['/login']);
    }
  }
  emailvalidation = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.getStates();
    this.getUserypes();
  }
  getStates() {
    this.PS.getState().subscribe(getStateData => {
      this.getStateData = getStateData;
    });
  }
  getUserypes() {
    this.PS.getUserType().subscribe(getUserData => {
      this.getUserData = getUserData;
      for(let i=0; i<this.getUserData.length;i++) {
        if (this.getUserData[i].userTypeId == this.userData['userDetails'].userTypeId) {
          this.type['userType'] = this.getUserData[i].userType;
        }
      }
      // this.type.userTypeId = this.getUserData.userType;
    });
  }
  cancelProfile() {
    this.router.navigate(['/home']);
  }
  errorFormFieldsArr = [];
  userProfileFormFields = [
    "username", "locationFlag", "password", "confirmPassword", "userFirstName", "userLastName",
    "UserStreet1", "userCity", "userZip", "userMobile", "email", "stateId", "userTypeId"
  ]
  editProfile() {

    this.errorFormFieldsArr = [];
    this.userProfileFormFields.forEach((item) => {
      if (item == 'locationFlag') {
        if (this.type['locationFlag'] == undefined || this.type['locationFlag'] == '' || this.type['locationFlag'] == '') {
          this.locationFlagshw = false;
        }
        if (this.type['locationFlag']) {
          if (this.type['location']) {
            this.locationFlagshw = false;
          } else {
            this.errorFormFieldsArr.push(item);
            this.locationFlagshw = true;
          }
    
        }
      } else {
        if (this.type[item] == null || this.type[item] == '') {
          this.errorFormFieldsArr.push(item);
          this[item + "shw"] = true;
        } else {
          this[item + "shw"] = false;
        }
      }
     
     
    });
    // if(this.type.password != this.type.confirmPassword){
    //   this.passwrdmatch = true;
    //   return;

    // }else{
    //   this.passwrdmatch = false;
    // }
    if (this.type.password != this.type.confirmPassword) {
      this.passwrdmatch = true;
      return;

    } else {
      this.passwrdmatch = false;
    }

    
    // if (this.type['locationFlag'] == undefined || this.type['locationFlag'] == '' || this.type['locationFlag'] == '') {
    //   this.locationFlagshw = false;
    // }
    // if (this.type['locationFlag']) {
    //   if (this.type['location']) {
    //     this.locationFlagshw = false;
    //   } else {
    //     this.locationFlagshw = true;
    //   }

    // }
    if (!this.emailvalidation.test(this.type.email)) {
      this.mailWrng = true;
      return;
    } else {
      this.mailWrng = false;
    }
    if (this.errorFormFieldsArr.length > 0) {
      return;
    };
    delete this.type.companyIdRelations;
    delete this.type.userName;
    this.type['userTypeId'] = this.userData['userDetails'].userTypeId;
    this.PS.editProfile(this.type).subscribe(getUserData => {
      this.getUserypes();
      this.getStates();
      this.successfulSave = true;
      this.successfulSave1 = false;
    });
  }
isDisabled = true;
  triggerSomeEvent() {
      this.isDisabled = !this.isDisabled;
      return;
  }
 
  isNumberKey(evt)
  {
    if(isNaN(evt)){
      this.type.userZip = ''
    }
  }
}
