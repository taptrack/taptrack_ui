import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailRoomReportComponent } from './mail-room-report.component';

describe('MailRoomReportComponent', () => {
  let component: MailRoomReportComponent;
  let fixture: ComponentFixture<MailRoomReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailRoomReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailRoomReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
